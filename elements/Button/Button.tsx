import React from 'react'
/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import { TButtonProps } from './Button.types'
import { spaces, fontSizes } from '../../style/global'
import styles from './Button.module.scss'

const { spaceS, space, spaceL } = spaces
const { fontSize, fontSizeS, fontSizeL } = fontSizes

const variantSwitch = (string: string) => {
  switch (string) {
    case 'small':
      return smallVariant
    case 'medium':
      return mediumVariant
    case 'large':
      return largeVariant
    case 'fullwidth':
      return fullWidthVariant
    default:
      return null
  }
}

const Button = ({
  children,
  labelColor = '#FFF',
  backgroundColor = '#000',
  onClick,
  isDisabled = false,
  size = 'medium',
}: TButtonProps): JSX.Element => {
  return (
    <div>
      <button
        className={styles.button}
        css={css`
          ${defaultStyle}
          ${variantSwitch(size)}
          cursor: ${isDisabled ? 'not-allowed' : 'pointer'};
          opacity: ${isDisabled ? 0.6 : 1};
          background-color: ${backgroundColor};
          color: ${labelColor}
        `}
        onClick={onClick}
      >
        {children}
      </button>
    </div>
  )
}

const defaultStyle = css`
  font-size: ${fontSize};
`

const smallVariant = css`
  font-size: ${fontSizeS};
  padding: 0 ${spaceS};
`

const mediumVariant = css`
  padding: ${spaceS} ${space};
`

const largeVariant = css`
  font-size: ${fontSizeL};
  padding: ${spaceS} ${spaceL};
`

const fullWidthVariant = css`
  display: block;
  width: 100%;
`
export default Button
