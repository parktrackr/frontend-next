import React from 'react'
import { Meta, Story } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button from './Button'
import { TButtonProps } from './Button.types'

export default {
  title: 'Element/Button',
  component: Button,
  argTypes: {
    children: { control: 'text' },
    backgroundColor: { control: 'color' },
    labelColor: { control: 'color' },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large', 'fullwidth'],
      },
    },
    isDisabled: { control: 'boolean' },
    onClick: { action: 'clicked' },
  },
} as Meta

const Template: Story<TButtonProps> = (args): JSX.Element => (
  <Button {...args}>{args.children}</Button>
)

export const Primary = Template.bind({})

Primary.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFFFFF',
  size: 'medium',
}

export const Secondary = Template.bind({})

Secondary.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#5882FA',
  labelColor: '#FFF',
  size: 'medium',
}

export const Large = Template.bind({})

Large.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFF',
  size: 'large',
}

export const Small = Template.bind({})

Small.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFF',
  size: 'small',
}

export const FullWidth = Template.bind({})

FullWidth.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFF',
  size: 'fullwidth',
}

export const Disabled = Template.bind({})

Disabled.args = {
  children: 'Button',
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFFFFF',
  size: 'medium',
  isDisabled: true,
}

/*
export const ContainsIcon = Template.bind({})

ContainsIcon.args = {
  children: <img src="/link.svg" alt="placeholder" />,
  onClick: action('clicked'),
  backgroundColor: '#FA5858',
  labelColor: '#FFFFFF',
  size: 'medium',
  containsIcon: true,
}
*/
