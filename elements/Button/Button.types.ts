export type TButtonProps = {
    children: React.ReactNode
    labelColor?: string
    backgroundColor?: string
    size?: string
    isDisabled: boolean
    onClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  }