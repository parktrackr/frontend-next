import React from 'react'
import { TBadgeProps } from './Badge.types'
import styles from './Badge.module.scss'

const Badge = ({
  children = 'Badge',
  onClick,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  status = 'neutral',
}: TBadgeProps): JSX.Element => {
  return (
    <>
      <div className={styles.badge} onClick={onClick}>
        {children}
      </div>
      <style jsx>
        {`
          .badge.positive {
            color: rgb(70, 124, 44);
            background: rgb(225, 255, 212) none repeat scroll 0% 0%;
          }
          .badge.negative {
            color: rgb(189, 53, 0);
            background: rgb(254, 222, 210) none repeat scroll 0% 0%;
          }
          .badge.neutral {
            color: rgb(102, 102, 102);
            background: rgb(238, 238, 238) none repeat scroll 0% 0%;
          }
          .badge.error {
            color: rgb(255, 255, 255);
            background: rgb(219, 59, 0) none repeat scroll 0% 0%;
          }
          .badge.warning {
            color: rgb(148, 99, 0);
            background: rgb(255, 244, 199) none repeat scroll 0% 0%;
          }
        `}
      </style>
    </>
  )
}

export default Badge
