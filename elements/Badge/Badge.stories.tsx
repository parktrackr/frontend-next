import React from 'react'
import { Meta, Story } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Badge from './Badge'
import { TBadgeProps } from './Badge.types'

export default {
  title: 'Element/Badge',
  component: Badge,
  argTypes: {
    children: { control: 'text' },
    status: {
      control: {
        type: 'select',
        options: ['positive', 'negative', 'neutral', 'error', 'warning'],
      },
    },
    onClick: { action: 'clicked' },
  },
} as Meta

const Template: Story<TBadgeProps> = (args): JSX.Element => (
  <Badge {...args}>{args.children}</Badge>
)

export const Basic = Template.bind({})

Basic.args = {
  onClick: action('clicked'),
}

export const All = (): JSX.Element => (
  <div>
    <Badge status="positive">Positive</Badge>
    <Badge status="negative">Negative</Badge>
    <Badge status="neutral">Neutral</Badge>
    <Badge status="error">Error</Badge>
    <Badge status="warning">Warning</Badge>
  </div>
)
