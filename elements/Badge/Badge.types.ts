export type TBadgeProps = {
  children: React.ReactNode
  status?: string
  onClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}
