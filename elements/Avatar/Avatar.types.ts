export type TAvatarProps = {
  children: React.ReactNode
  size?: string
  src?: string
  onClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}
