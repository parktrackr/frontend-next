import React from 'react'
import { TAvatarProps } from './Avatar.types'
import styles from './Avatar.module.scss'

const Avatar = ({
  children,
  size = 'medium',
  src,
}: TAvatarProps): JSX.Element => {
  return (
    <>
      <div className={styles.avatar}>
        {src ? (
          <img className={'image'} src={src} />
        ) : (
          <div className={`avatarInner ${size}`}>{children}</div>
        )}
      </div>
      <style jsx>
        {`
          .avatar.small {
            height: 20px;
            width: 20px;
            lineheight: 20px;
          }
          .avatar.medium {
            height: 30px;
            width: 30px;
            lineheight: 30px;
          }
          .avatar.large {
            height: 40px;
            width: 40px;
            lineheight: 40px;
          }
          .avatarInner {
            text-align: center;
            color: #fff;
            box-sizing: border-box;
          }
          .avatarInner.small {
            font-size: 14px;
            line-height: 20px;
          }
          .avatarInner.medium {
            font-size: 16px;
            line-height: 28px;
          }
          .avatarInner.large {
            font-size: 20px;
            line-height: 40px;
          }
          .image {
            width: 100%;
            height: auto;
            display: block;
            box-sizing: border-box;
          }
        `}
      </style>
    </>
  )
}

export default Avatar
