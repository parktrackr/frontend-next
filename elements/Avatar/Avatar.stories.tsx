import React from 'react'
import { Meta, Story } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Avatar from './Avatar'
import { TAvatarProps } from './Avatar.types'

export default {
  title: 'Element/Avatar',
  component: Avatar,
  argTypes: {
    children: { control: 'text' },
    src: { control: 'text' },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
    onClick: { action: 'clicked' },
  },
} as Meta

const Template: Story<TAvatarProps> = (args): JSX.Element => (
  <Avatar {...args}>{args.children}</Avatar>
)

export const Primary = Template.bind({})

Primary.args = {
  children: 'L',
  onClick: action('clicked'),
  size: 'medium',
}

export const Secondary = Template.bind({})

Secondary.args = {
  children: 'L',
  onClick: action('clicked'),
  size: 'medium',
}

export const Large = Template.bind({})

Large.args = {
  children: 'L',
  onClick: action('clicked'),
  size: 'large',
}

export const Small = Template.bind({})

Small.args = {
  children: 'L',
  onClick: action('clicked'),
  size: 'small',
}

export const Disabled = Template.bind({})

Disabled.args = {
  children: 'L',
  onClick: action('clicked'),
  size: 'medium',
}
