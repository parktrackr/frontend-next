import React from 'react'
import { Meta, Story } from '@storybook/react'
import Input from './Input'
import { TInputProps } from './Input.types'

export default {
  title: 'Element/Input',
  component: Input,
  argTypes: {
    placeholder: { control: 'text' },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
  },
} as Meta

const Template: Story<TInputProps> = (args): JSX.Element => <Input {...args} />

export const Primary = Template.bind({})

Primary.args = {
  size: 'medium',
}

export const Secondary = Template.bind({})

Secondary.args = {
  size: 'medium',
}

export const Large = Template.bind({})

Large.args = {
  size: 'large',
}

export const Small = Template.bind({})

Small.args = {
  size: 'small',
}
