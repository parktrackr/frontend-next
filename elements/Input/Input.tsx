import React from 'react'
import styles from './Input.module.scss'
import { TInputProps } from './Input.types'

const renderSize = (size: string) => {
  switch (size) {
    case 'small':
      return {
        height: '2rem',
        fontSize: '0.875rem',
      }
    case 'medium':
      return {
        fontSize: '1rem',
        height: '2.5rem',
      }
    case 'large':
      return {
        fontSize: '1.25rem',
        height: '3rem',
      }
    default:
      return {}
  }
}

const Input = ({
  size = 'medium',
  placeholder = 'placeholder',
}: TInputProps): JSX.Element => {
  const sizeProperties = renderSize(size)
  return (
    <input
      className={styles.input}
      style={{ ...sizeProperties }}
      placeholder={placeholder}
      aria-label="placeholder"
    />
  )
}

export default Input
