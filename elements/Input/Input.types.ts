export type TInputProps = {
  size?: string
  placeholder?: string
}
