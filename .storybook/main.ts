module.exports = {
  stories: [
    '../stories/*.stories.@(ts|tsx|js|jsx|mdx)',
    '../components/**/*.stories.tsx',
    '../elements/**/*.stories.tsx',
  ],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-docs',
    '@storybook/addon-controls',
    '@storybook/addon-a11y',
    '@storybook/addon-storysource',
    '@storybook/addon-viewport',
    '@storybook/addon-backgrounds',
    '@storybook/preset-scss',
  ],
}
