import Head from 'next/head'

export const siteTitle = 'Next.js Sample Website'

const Layout = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <div>
      <Head>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header>
        <>
          <h2>Header</h2>
        </>
      </header>
      <main>{children}</main>
      <p>Under main</p>
    </div>
  )
}

export default Layout
