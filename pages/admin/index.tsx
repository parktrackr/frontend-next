import Head from 'next/head'
import Layout from '../../components/layout'

const Admin = (): JSX.Element => {
  return (
    <Layout>
      <Head>
        <title>Admin</title>
      </Head>
      <h1>First Post</h1>
    </Layout>
  )
}

export default Admin
