# ParkTrackr

ParkTrackr is an amusement park platform on which the experience of visiting, experiencing and the excitement for amusement park is being enhanced.

[![pipeline status](https://gitlab.com/parktrackr/frontend-next/badges/develop/pipeline.svg)](https://gitlab.com/parktrackr/frontend-next/-/commits/develop)

![node-current](https://img.shields.io/node/v/12.18.4)
