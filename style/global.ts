const variables = {
  defaultSpace: 16,
  defaultFontSize: 16,
  defaultBaseLineHeight: 24,
}

const { defaultSpace, defaultFontSize } = variables

const spaces = {
  space: `${defaultSpace}px`,
  spaceXS: `${defaultSpace / 4}px`,
  spaceS: `${defaultSpace / 2}px`,
  spaceL: `${defaultSpace * 2}px`,
  spaceXL: `${defaultSpace * 4}px`,
}

const fontSizes = {
  fontSize: `${defaultFontSize}px`,
  fontSizeXS: `${defaultFontSize / 4}px`,
  fontSizeS: `${defaultFontSize / 2}px`,
  fontSizeL: `${defaultFontSize * 2}px`,
  fontSizeXL: `${defaultFontSize * 4}px`,
}

export { spaces, fontSizes }
